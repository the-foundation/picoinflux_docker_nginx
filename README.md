# Picoinflux_docker_nginx

Measure timings of web pageload

`apt-get install dbus-x11`

## CRON:
```
23 */5   * * *  /usr/bin/ionice -c 3 /usr/bin/nice -n 19 /usr/bin/cpulimit -q -c 1 -l 30 -f -m -- /bin/bash /etc/custom/picoinflux_pageload/picoinflux_pageload.sh 2>&1 |grep -v -e " detected" -e " dead" -e "No process found" -e "Child process is finished"
```

---

<a href="https://the-foundation.gitlab.io/">
<h3>A project of the foundation</h3>
<div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/picoinflux_docker_nginx/README.md/logo.jpg" width="480" height="270"/></div></a>
