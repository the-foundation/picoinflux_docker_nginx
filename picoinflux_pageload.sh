#!/bin/bash

#!/bin/sh
##FIND MYSELF
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

test -f /tmp/picopageload.ff.log && rm /tmp/picopageload.ff.log
test -f /tmp/picopageload.python.log && rm /tmp/picopageload.python.log

# TARGET FORMAT  : load_shortterm,host=SampleClient value=0.67
# CREATE ~/.picoinflux.conf with first line user:pass second line url (e.g. https://influxserver.net:8086/write?db=collectd
# ADDTIONALY set custom hostname in /etc/picoinfluxid
# PLACE all domains to be tested in /etc/picopagedomains ( ONE PER LINE PLZ )
hostname=$(cat /etc/picoinfluxid 2>/dev/null || (which hostname >/dev/null && hostname -f || (which uci >/dev/null && uci show |grep ^system|grep hostname=|cut -d\' -f2 ))) 2>/dev/null
cd $DIR
test -d selenium-pageload/venv||(echo "fail, no venv" >&2;exit 1)
which geckodriver >/dev/null || (echo "fail, no geckodriver" >&2;exit 1)

#NGINX_NAME=nginx ;
#( docker exec -t $NGINX_NAME nginx -T -c /etc/nginx/nginx.conf 2>&1 | grep "server_name " | grep -v "This is just an invalid value" | sed 's/\r//g;s/^\(\t\| \)\+//g;s/\(server_name \|;\)//g' | sort | uniq | grep -v "^www\." | while read a; do     curl -kvL https://$a/ 2>&1 | grep "^< HTTP/" | grep -q -e 502 -e 503 -e 504 && echo "web_pageload_"$a"=-1" || ( TIMEFORMAT='%E'; echo "web_pageload_"$a"="$(time (echo -n "https://""$a"|wget  --no-cache -K -p   -e robots=off --max-redirect 2 -i- -O/tmp/pageload.out --timeout=8  &>/dev/null ) 2>&1) | sed 's/\r//g;s/,/./g' );sleep 1; done ) | grep -v -e ^$ -e =$ |sed 's/=/,host='"$hostname"' value=/g' 2>/dev/null > ~/.influxpageloaddata
#( docker exec -t $NGINX_NAME nginx -T -c /etc/nginx/nginx.conf 2>&1 | grep "server_name " | grep -v "This is just an invalid value" | sed 's/\r//g;s/^\(\t\| \)\+//g;s/\(server_name \|;\)//g' 
( cat /etc/picopagedomains | sort | uniq | grep -v "^www\." | while read a; do     
							curl -kvL https://$a/ 2>&1 |tee /tmp/picopage.current| grep "^< HTTP/" | grep -q -e 502 -e 503 -e 504 && echo "web_pageload_"$a"=-1" || ( su -s /bin/bash -c "cd $DIR;cd selenium-pageload;pwd;test -d venv ||  virtualenv venv ; source venv/bin/activate ;pip install -r requirements.txt ;python pageload.py -u http://$a --headless ;kill -9 $(pidof Xvfb firefox 2>>/dev/null);kill -9 $(pidof firefox firefox-esr geckodriver xvfb python 2>/dev/null) &>>/tmp/picopageload.python.log ;mv *.log /tmp/;find /tmp/ -user selenium -delete" selenium 2>&1|tee -a /tmp/picopageload.python.log |grep -e ^loadtime -e ^backend |while read res;do 
							 echo "web_pageload_"$a":"$res;done|sed 's/:loadtime:/=/g;s/:backend:/_backend=/g' )   |grep -v -e ^$ -e =$ ;done ) |while read out;do 
								echo "$out "$(date -u +%s%N) |sed 's/=/,host='"$hostname"' value=/g' ;sleep 2 ;done 2>/tmp/influxpageloadtime.error|while read result;do 
										echo "$result" > ~/.influxpageloaddata ; (
											 curl -s -k -u $(head -n1 ~/.picoinflux.conf) -i -XPOST "$(head -n2 ~/.picoinflux.conf|tail -n1)" --data-binary @$HOME/.influxpageloaddata 2>&1 && rm  $HOME/.influxpageloaddata 2>&1 ) >/tmp/picoinfluxpageload.log & 
									done
#mv *.log /tmp/
